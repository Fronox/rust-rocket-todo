#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;

mod schema;
mod todos;

use rocket::Rocket;

#[get("/")]
fn index() -> &'static str {
    "Hello!"
}

fn rocket() -> Rocket {
    let base_routes = routes![index];

    let todo_routes = routes![
        todos::routes::list_todos,
        todos::routes::get_todo,
        todos::routes::create_todo
    ];

    rocket::ignite()
        .attach(todos::db::DbConn::fairing())
        .mount("/", base_routes)
        .mount("/todos", todo_routes)
}

fn main() {
    rocket().launch();
}
