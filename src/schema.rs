table! {
    todo (id) {
        id -> Uuid,
        title -> Varchar,
        description -> Varchar,
    }
}
