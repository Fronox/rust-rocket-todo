use super::db::DbConn;
use crate::schema::todo::{self, dsl::*};

use diesel::{prelude::*, Insertable, Queryable, RunQueryDsl};
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Queryable, Serialize)]
pub struct Todo {
    pub id: Uuid,
    pub title: String,
    pub description: String,
}

impl Todo {
    pub fn get(conn: DbConn, todo_id: uuid::Uuid) -> Todo {
        todo.filter(id.eq(todo_id)).first(&*conn).unwrap()
    }

    pub fn create(conn: DbConn, new_todo: NewTodo) -> Todo {
        diesel::insert_into(todo::table)
            .values(&new_todo)
            .get_result(&*conn)
            .unwrap()
    }

    pub fn list(conn: DbConn) -> Vec<Todo> {
        todo.load(&*conn).unwrap()
    }
}

#[derive(Insertable, Deserialize)]
#[table_name = "todo"]
pub struct NewTodo {
    pub title: String,
    pub description: String,
}
