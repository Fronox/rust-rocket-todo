use super::{
    db::DbConn,
    models::{NewTodo, Todo},
};
use rocket_contrib::json::Json;

#[get("/")]
pub fn list_todos(conn: DbConn) -> Json<Vec<Todo>> {
    let todo_list = Todo::list(conn);
    Json(todo_list)
}

#[get("/<id>")]
pub fn get_todo(conn: DbConn, id: String) -> Json<Todo> {
    let id_uuid = uuid::Uuid::parse_str(id.as_str()).unwrap();
    let todo = Todo::get(conn, id_uuid);
    Json(todo)
}

#[post("/", data = "<new_todo>")]
pub fn create_todo(conn: DbConn, new_todo: Json<NewTodo>) -> Json<Todo> {
    let todo = Todo::create(conn, new_todo.into_inner());
    Json(todo)
}
